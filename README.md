# Craft CMS Template

## Usage in new projects
The recommended way is to fork this project into your new project as this allows for merge requests at a later stage and in general makes changes more traceable.

## Installation
If in doubt, check the official docs https://craftcms.com/docs/3.x/installation.html#step-1-download-craft

### Netcup 
1. Clone the forked repository (might make sense to use `--depth 1` to only load the most recent commit).
2. Setup the composer packages in the netcup UI and install them.
3. Copy/rename the .env.example file to .env
4. Run security setup via SSH commandline `php craft setup/security-key`
5. Create a DB if it doesn't exist already
6. Run `php craft setup` via SSH commandline and follow the prompts
7. Make sure you set the env variables in [environment setup](#environment-setup)

Alternatively to step 4. and 6. you can fill in the variables in the .env file manually.
For the security key use a cryptographic key (1Password generator).
Afterwards you should be prompted to install craft if you try to visit the admin panel

### VPS
1. Clone the forked repository (might make sense to use `--depth 1` to only load the most recent commit).
2. In the repo folder run `composer install`
3. Copy/rename the .env.example file to .env
4. Run security setup via SSH commandline `php craft setup/security-key` (alternatively set it manually in .env)
5. Create a DB if it doesn't exist already
6. Run `php craft setup` via SSH commandline and follow the prompts
7. Make sure you set the env variables in [environment setup](#environment-setup)



## Environment Setup
The Craft installation script copies the .env.example file and via CLI setup fills in fields like DB connection and security keys. 
We still need to take care of some additional variables:
```bash
 ASSETS_BASE_URL=https://my-cms-url.de/assets    # This URL assumes that the main assets volume in craft is under the folder *assets*
 BASE_CP_URL=https://my-cms-url.de               # This is used by craft for things like password reset links and so on
 PRIMARY_SITE_URL=https://my-frontend-url.de     # We use this to configure preview targets on sections via the alias (see config/general.php) @frontend   

```

## Asset Volume Setup
Generally we differentiate between publicly available volumes and ones that are not accessible via public URLs.
To make that distinction there is a craft setting, but the rule of thumb should be anyways, that
Private volumes should live above the webroot, e.g. `/craftfolder/storage/backups`.

Publicly accessible volumes live in the `/web` folder and we normally use the `/web/assets` for the main asset volume.
We need to set some additional settings when creating a volume in craft to make sure the assets in this volume are reachable:

```
Base-URL: $ASSETS_BASE_URL
Verzeichnis des Dateisystems: @webroot/assets
```


## Mail Setup
In order for craft to be able to send Invitation and Password Reset mails you have to set up a mail service (SMTP works quite well) in Settings/Mail.

## Common Plugins
The repo includes commonly used plugins, those plugins however still need to be installed via the Control Panel: Settings/Plugins

## Trouble Shooting
- If you get a 404 Error for the admin panel it's most likely that your BASE_CP_URL is not configured correctly in .env
- If you get a 503 Service unavailable Error, make sure that the .env file exists


## Setup Local Dev Env
1. Install **ddev** on yout sytem: https://ddev.readthedocs.io/en/stable/#installation
2. Rename `.env.local` to `.env`
3. in the root of the repo run: `ddev start`
4. Open `https://staging.craft-template.ddev.site/admin/` in the browser and install craft. 

**For the sake of consistency, the admin user should read as follows:**
```
username: admin
password: password
e-mail: it@neesh.de
```
5. Hopefully done: 😬
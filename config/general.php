<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

use craft\helpers\App;

$isDev = App::env('ENVIRONMENT') === 'dev';
$isProd = App::env('ENVIRONMENT') === 'production';

return [
    // Default Week Start Day (0 = Sunday, 1 = Monday...)
    'defaultWeekStartDay' => 1,

    // Whether generated URLs should omit "index.php"
    'omitScriptNameInUrls' => true,

    // The URI segment that tells Craft to load the control panel
    'cpTrigger' => App::env('CP_TRIGGER') ?: 'admin',

    // The secure key Craft will use for hashing and encrypting data
    'securityKey' => App::env('SECURITY_KEY'),

    // Whether Dev Mode should be enabled (see https://craftcms.com/guides/what-dev-mode-does)
    'devMode' => $isDev,

    // Whether administrative changes should be allowed
    'allowAdminChanges' => $isDev,

    // Whether crawlers should be allowed to index pages and following links
    'disallowRobots' => !$isProd,

    // Additional settings

    // Add additional allowed file extensions (sql is needed for the "DB Dump" plugin to work)
    'extraAllowedFileExtensions' => 'sql',

    // A value of 0 sets the cache indefinitely. it's not clear from the docs if this cache duration applies also to image transforms, but that would be the goal with this setting
    'cacheDuration' => 0,

    'headlessMode' => true,

    // Set teh baseCpUrl to make sure that password reset links use the backend URL defined in .env
    'baseCpUrl' => craft\helpers\App::env('BASE_CP_URL'),

    // For live preview
    'tokenParam' => 'token',

    // new setting in craft 3.7 to revision asset urls when they are changed (e.g. focal point)
    'revAssetUrls' => true,

    'aliases' => [
        '@frontend' => craft\helpers\App::env('PRIMARY_SITE_URL'),
    ],

    // Increase max upload size
    // 'maxUploadFileSize' => 35777216, 

];
